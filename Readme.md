# Serverless backend application

### Requirements

- serverless framework
- AWS credentials configuration

### Installation

To get started:

```bash
npm install -g serverless
```

### Deploy

```bash
serverless deploy
```

### This project going to create

- A Sample python3.6 lambda function
- Api Gateway endpoint wich handle get requests to lambda.

### Configure aws variables in order to run cloudformation stack

- Set Follow variables
    - AWS_ACCESS_KEY_ID
    - AWS_DEFAULT_REGION
    - AWS_SECRET_ACCESS_KEY
    
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html